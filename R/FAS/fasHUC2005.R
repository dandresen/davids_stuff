#2005 
#turns off scientific notation 
options(scipen=999)
#import the needed packages 
library(sqldf)
library(XLConnect)
options(java.parameters = "-Xmx1024m")
#set the working directory (the folder you are working out of)
setwd("D:/FAShuc")
#give the main file a name (files with %'s)
main <- read.csv ("main2005.txt")
#load the water use by county excel file- name it county20XX
county2005 <- loadWorkbook("Export_2005, Texas_County.xlsx", create = F)

#reads each worksheet in the excel file, renames the columns to take out the "-" 
#then multiplies the ratio of the county in each HUC- derived in ArcMap- to the water variable and finally sorts on the HUC 
#exports to the excel file - further instructions at the bottom
#will take at least two runs due to lack of memory 

TP <- readWorksheet(county2005,"TP",4,1,258,2, header = T)
names(TP)[names(TP)=="TP.TotPop"] <- "TPtotpop"

HUC2005TP <- sqldf("SELECT main.HUC8 AS Area
                          , sum(main.Ratio * TP.tptotpop) AS TP_TotPop
                   FROM main INNER JOIN TP on main.FIPS = TP.Area
                   GROUP BY main.HUC8")

PS <- readWorksheet(county2005,"PS",4,1,258,8, header = T)
names(PS)[names(PS)=="PS.TOPop"] <- "pstopop"
names(PS)[names(PS)=="PS.WGWFr"] <- "pswgwfr"
names(PS)[names(PS)=="PS.WGWSa"] <- "pswgwsa"
names(PS)[names(PS)=="PS.WSWFr"] <- "pswswfr"
names(PS)[names(PS)=="PS.WSWSa"] <- "pswswsa"
names(PS)[names(PS)=="PS.RecWW"] <- "psrecww"
names(PS)[names(PS)=="PS.Facil"] <- "psfacil"

HUC2005PS <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * PS.pstopop) AS PS_TOPop
                         , sum(main.Ratio * PS.pswgwfr) AS PS_WGWFr
                         , sum(main.Ratio * PS.pswgwsa) AS PS_WGWSa
                         , sum(main.Ratio * PS.pswswfr) AS PS_WSWFr
                         , sum(main.Ratio * PS.pswswsa) AS PS_WSWsa
                         , sum(main.Ratio * PS.psrecww) AS PS_RecWW
                         , sum(main.Ratio * PS.psfacil) AS PS_Facil
                   FROM main INNER JOIN PS on main.FIPS = PS.Area
                   GROUP BY main.HUC8")

CO <- readWorksheet(county2005,"CO",4,1,258,6, header = T)
names(CO)[names(CO)=="CO.WGWFr"] <- "cowgwfr"
names(CO)[names(CO)=="CO.WSWFr"] <- "cowswfr"
names(CO)[names(CO)=="CO.PSDel"] <- "copsdel"
names(CO)[names(CO)=="CO.CUsFr"] <- "cocusfr"
names(CO)[names(CO)=="CO.RecWW"] <- "corecww"

HUC2005CO <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * CO.cowgwfr) AS CO_WGWFr
                         , sum(main.Ratio * CO.cowswfr) AS CO_WSWFr
                         , sum(main.Ratio * CO.copsdel) AS CO_PSDel
                         , sum(main.Ratio * CO.cocusfr) AS CO_CUsFr
                         , sum(main.Ratio * CO.corecww) AS CO_Recww
                   FROM main INNER JOIN CO on main.FIPS = CO.Area
                   GROUP BY main.HUC8")

DO <- readWorksheet(county2005,"DO",4,1,258,5, header = T)
names(DO)[names(DO)=="DO.WGWFr"] <- "dowgwfr"
names(DO)[names(DO)=="DO.WSWFr"] <- "dowswfr"
names(DO)[names(DO)=="DO.PSDel"] <- "dopsdel"
names(DO)[names(DO)=="DO.CUsFr"] <- "docusfr"

HUC2005DO <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * DO.dowgwfr) AS DO_WGWFr
                         , sum(main.Ratio * DO.dowswfr) AS DO_WSWFr
                         , sum(main.Ratio * DO.dopsdel) AS DO_PSDel
                         , sum(main.Ratio * DO.docusfr) AS DO_CUsFr
                   FROM main INNER JOIN DO on main.FIPS = DO.Area
                   GROUP BY main.HUC8")

#had to name the IN tab In1- R does not like IN 
In1 <- readWorksheet(county2005,"IN",4,1,258,10, header = T)
names(In1)[names(In1)=="IN.WGWFr"] <- "Inwgwfr"
names(In1)[names(In1)=="IN.WGWSa"] <- "Inwgwsa"
names(In1)[names(In1)=="IN.WSWFr"] <- "Inwswfr"
names(In1)[names(In1)=="IN.WSWSa"] <- "Inwswsa"
names(In1)[names(In1)=="IN.PSDel"] <- "Inpsdel"
names(In1)[names(In1)=="IN.CUsFr"] <- "Incusfr"
names(In1)[names(In1)=="IN.CUsSa"] <- "Incussa"
names(In1)[names(In1)=="IN.RecWW"] <- "Inrecww"
names(In1)[names(In1)=="IN.Facil"] <- "Infacil"

HUC2005In <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * In1.Inwgwfr) AS IN_WGWFr
                         , sum(main.Ratio * In1.Inwgwsa) AS IN_WGWSa
                         , sum(main.Ratio * In1.Inwswfr) AS IN_WSWfR
                         , sum(main.Ratio * In1.Inwswsa) AS IN_WSWSa
                         , sum(main.Ratio * In1.Inpsdel) AS IN_PSDel
                         , sum(main.Ratio * In1.Incusfr) AS IN_CUsFr
                         , sum(main.Ratio * In1.Incussa) AS IN_CUsSa
                         , sum(main.Ratio * In1.Inrecww) AS IN_RecWW
                         , sum(main.Ratio * In1.Infacil) AS IN_Facil
                   FROM In1 INNER JOIN main on main.FIPS = In1.Area
                   GROUP BY main.HUC8")

PO <-  readWorksheet(county2005,"PO",4,1,258,11, header = T)
names(PO)[names(PO)=="PO.WGWFr"] <- "POwgwfr"
names(PO)[names(PO)=="PO.WGWSa"] <- "POwgwsa"
names(PO)[names(PO)=="PO.WSWFr"] <- "POwswfr"
names(PO)[names(PO)=="PO.WSWSa"] <- "POwswsa"
names(PO)[names(PO)=="PO.PSDel"] <- "POpsdel"
names(PO)[names(PO)=="PO.CUsFr"] <- "POcusfr"
names(PO)[names(PO)=="PO.CUsSa"] <- "POcussa"
names(PO)[names(PO)=="PO.Power"] <- "POpower"
names(PO)[names(PO)=="PO.RecWW"] <- "POrecww"
names(PO)[names(PO)=="PO.Facil"] <- "POfacil"

HUC2005PO <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * PO.POwgwfr) AS PO_WGWFr
                         , sum(main.Ratio * PO.POwgwsa) AS PO_WGWSa
                         , sum(main.Ratio * PO.POwswfr) AS PO_WSWFr
                         , sum(main.Ratio * PO.POwswsa) AS PO_WSWSa
                         , sum(main.Ratio * PO.POpsdel) AS PO_PSDel
                         , sum(main.Ratio * PO.POcusfr) AS PO_CUsFr
                         , sum(main.Ratio * PO.POcussa) AS PO_CUsSa
                         , sum(main.Ratio * PO.POpower) AS PO_Power
                         , sum(main.Ratio * PO.POrecww) AS PO_RecWW
                         , sum(main.Ratio * PO.POfacil) AS PO_Facil
                   FROM PO INNER JOIN main on main.FIPS = PO.Area
                   GROUP BY main.HUC8")

PC <- readWorksheet(county2005,"PC",4,1,258,11, header = T)
names(PC)[names(PC)=="PC.WGWFr"] <- "PCwgwfr"
names(PC)[names(PC)=="PC.WGWSa"] <- "PCwgwsa"
names(PC)[names(PC)=="PC.WSWFr"] <- "PCwswfr"
names(PC)[names(PC)=="PC.WSWSa"] <- "PCwswsa"
names(PC)[names(PC)=="PC.PSDel"] <- "PCpsdel"
names(PC)[names(PC)=="PC.CUsFr"] <- "PCcusfr"
names(PC)[names(PC)=="PC.CUsSa"] <- "PCcussa"
names(PC)[names(PC)=="PC.Power"] <- "PCpower"
names(PC)[names(PC)=="PC.RecWW"] <- "PCrecww"
names(PC)[names(PC)=="PC.Facil"] <- "PCfacil"

HUC2005PC <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * PC.PCwgwfr) AS PC_WGWFr
                         , sum(main.Ratio * PC.PCwgwsa) AS PC_WGWSa
                         , sum(main.Ratio * PC.PCwswfr) AS PC_WSWFr
                         , sum(main.Ratio * PC.PCwswsa) AS PC_WSWSa
                         , sum(main.Ratio * PC.PCpsdel) AS PC_PSDel
                         , sum(main.Ratio * PC.PCcusfr) AS PC_CUsFr
                         , sum(main.Ratio * PC.PCcussa) AS PC_CUsSa
                         , sum(main.Ratio * PC.PCpower) AS PC_Power
                         , sum(main.Ratio * PC.PCrecww) AS PC_RecWW
                         , sum(main.Ratio * PC.PCfacil) AS PC_Facil
                   FROM PC INNER JOIN main on main.FIPS = PC.Area
                   GROUP BY main.HUC8")

MI <- readWorksheet(county2005,"MI",4,1,258,8, header = T)
names(MI)[names(MI)=="MI.WGWFr"] <- "MIwgwfr"
names(MI)[names(MI)=="MI.WGWSa"] <- "MIwgwsa"
names(MI)[names(MI)=="MI.WSWFr"] <- "MIwswfr"
names(MI)[names(MI)=="MI.WSWSa"] <- "MIwswsa"
names(MI)[names(MI)=="MI.CUsFr"] <- "MIcusfr"
names(MI)[names(MI)=="MI.CUsSa"] <- "MIcussa"
names(MI)[names(MI)=="MI.RecWW"] <- "MIrecww"

HUC2005MI <- sqldf("SELECT main.HUC8 As Area
                         , sum(main.Ratio * MI.MIwgwfr) AS MI_WGWFr
                         , sum(main.Ratio * MI.MIwgwsa) AS MI_WSWSa
                         , sum(main.Ratio * MI.MIwswfr) AS MI_WSWFr
                         , sum(main.Ratio * MI.MIwswsa) AS MI_WSWSa
                         , sum(main.Ratio * MI.MIcusfr) AS MI_CUsFr
                         , sum(main.Ratio * MI.MIcussa) AS MI_CUsSa
                         , sum(main.Ratio * MI.MIrecww) AS MI_RecWW
                   FROM MI INNER JOIN main on main.FIPS = MI.Area
                   GROUP BY main.HUC8")

LI <- readWorksheet(county2005,"LI",4,1,258,4, header = T)
names(LI)[names(LI)=="LI.WGWFr"] <- "LIwgwfr"
names(LI)[names(LI)=="LI.WSWFr"] <- "LIwswfr"
names(LI)[names(LI)=="LI.CUsFr"] <- "LIcusfr"

HUC2005LI <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * LI.LIwgwfr) AS LI_WGWFr
                         , sum(main.Ratio * LI.LIwswfr) AS LI_WSWFr
                         , sum(main.Ratio * LI.LIcusfr) AS LI_CUsFr
                   FROM LI INNER JOIN main on main.FIPS = LI.Area
                   GROUP BY main.HUC8")

AQ <- readWorksheet(county2005,"AQ",4,1,258,7, header = T)
names(AQ)[names(AQ)=="AQ.WGWFr"] <- "AQwgwfr"
names(AQ)[names(AQ)=="AQ.WGWSa"] <- "AQwgwsa"
names(AQ)[names(AQ)=="AQ.WSWFr"] <- "AQwswfr"
names(AQ)[names(AQ)=="AQ.WSWSa"] <- "AQwswsa"
names(AQ)[names(AQ)=="AQ.CUsFr"] <- "AQcusfr"
names(AQ)[names(AQ)=="AQ.CUsSa"] <- "AQcussa"

HUC2005AQ <- sqldf("SELECT main.HUC8 AS Area
                         , sum(main.Ratio * AQ.AQwgwfr) AS AQ_WGWFr
                         , sum(main.Ratio * AQ.AQwgwsa) AS AQ_WGWSa
                         , sum(main.Ratio * AQ.AQwswfr) AS AQ_WSWFr
                         , sum(main.Ratio * AQ.AQwswsa) AS AQ_WSWSa
                         , sum(main.Ratio * AQ.AQcusfr) AS AQ_CUsFr
                         , sum(main.Ratio * AQ.AQcussa) AS AQ_CUsSa
                   FROM AQ INNER JOIN main on main.FIPS = AQ.Area
                   GROUP BY main.HUC8")

IR <- readWorksheet(county2005,"IR",4,1,258,9, header = T)
names(IR)[names(IR)=="IR.WGWFr"] <- "IRwgwfr"
names(IR)[names(IR)=="IR.WSWFr"] <- "IRwswfr"
names(IR)[names(IR)=="IR.CUsFr"] <- "IRcusfr"
names(IR)[names(IR)=="IR.CLoss"] <- "IRcloss"
names(IR)[names(IR)=="IR.IrSpr"] <- "IRirspr"
names(IR)[names(IR)=="IR.IrMic"] <- "IRirmic"
names(IR)[names(IR)=="IR.IrSur"] <- "IRirsur"
names(IR)[names(IR)=="IR.RecWW"] <- "IRrecww"

HUC2005IR <- sqldf("SELECT main.HUC8 As Area
                         , sum(main.Ratio * IR.IRwgwfr) AS IR_WGWFr
                         , sum(main.Ratio * IR.IRwswfr) AS IR_WSWFr
                         , sum(main.Ratio * IR.IRcusfr) AS IR_CUsFr
                         , sum(main.Ratio * IR.IRcloss) AS IR_CLoss
                         , sum(main.Ratio * IR.IRirspr) AS IR_IrSpr
                         , sum(main.Ratio * IR.IRirmic) AS IR_IrMic
                         , sum(main.Ratio * IR.IRirsur) AS IR_IrSur
                         , sum(main.Ratio * IR.IRrecww) AS IR_RecWW
                   FROM IR INNER JOIN main on main.FIPS = IR.Area
                   GROUP BY main.HUC8")

HY <- readWorksheet(county2005,"HY",4,1,258,7, header = T)
names(HY)[names(HY)=="HY.InUse"] <- "HYInUse"
names(HY)[names(HY)=="HY.OffFr"] <- "HYOffFr"
names(HY)[names(HY)=="HY.InPow"] <- "HYInPow"
names(HY)[names(HY)=="HY.OfPow"] <- "HYOfPow"
names(HY)[names(HY)=="HY.InFac"] <- "HYInfac"
names(HY)[names(HY)=="HY.OfFac"] <- "HYOfFac"


HUC2005HY <- sqldf("SELECT main.HUC8 AS Area
                   , sum(main.Ratio * HY.HYInuse) AS HY_Inuse
                   , sum(main.Ratio * HY.HYOffFr) AS HY_OffFr
                   , sum(main.Ratio * HY.HYInPow) AS HY_InPow
                   , sum(main.Ratio * HY.HYOfPow) AS HY_OfPow
                   , sum(main.Ratio * HY.HYInfac) AS HY_Infac
                   , sum(main.Ratio * HY.HYOfFac) AS HY_OfFac
                   FROM HY INNER JOIN main on main.FIPS = HY.Area
                   GROUP BY main.HUC8")

WW <- readWorksheet(county2005,"WW",4,1,258,4, header = T)
names(WW)[names(WW)=="WW.PuRet"] <- "WWpuret"
names(WW)[names(WW)=="WW.PuFac"] <- "WWpufac"
names(WW)[names(WW)=="WW.RecWW"] <- "WWrecww"

HUC2005WW <- sqldf("SELECT main.HUC8 AS Area
                   , sum(main.Ratio * WW.WWpuret) AS WW_PuRet
                   , sum(main.Ratio * WW.WWpufac) AS WW_PuFac
                   , sum(main.Ratio * WW.WWrecww) AS WW_RecWW
                   FROM WW INNER JOIN main on main.FIPS = WW.Area
                   GROUP BY main.HUC8")

#loads the results into the excel file- will turn column titles grey- Split into 2 runs because of memory issue
#be sure to comment out the one of the "if(FALSE) and the "{}"s 
#1st Run
#if(FALSE){
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005TP, name = "TP", formula = "TP!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005PS, name = "PS", formula = "PS!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005CO, name = "CO", formula = "CO!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005DO, name = "DO", formula = "DO!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005In, name = "IN", formula = "IN!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005PO, name = "PO", formula = "PO!$A$4")
#}

#2nd Run
#if(FALSE){
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005PC, name = "PC", formula = "PC!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005MI, name = "MI", formula = "MI!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005LI, name = "LI", formula = "LI!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005AQ, name = "AQ", formula = "AQ!$A$4")
writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005IR, name = "IR", formula = "IR!$A$4")
#writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005HY, name = "HY", formula = "HY!$A$4") Values are zero
#writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005WW, name = "WW", formula = "WW!$A$4") Values are zero 
#}
#writeNamedRegionToFile("TxHUC8WaterData2005NEW.xlsx", HUC2005RE, name = RE, formula = "RE!$A$4")





