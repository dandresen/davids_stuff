# The following tool can be used to create a table which provides the percentage of a given HUC in a given County

# The first step is download appropriate HUC and County layers for the year in question
# Then create a new gdb that has the downloaded HUC and County layers as feature classes 
# This tool uses the FIPS number to identify each County, it gets the FIPS from the last 3 characters in the GEOID field

# The output of this tool will be a .txt (or .csv) file that can then be used with the County water use table to
# determine water use levels at the HUC scale 

# Methods developed by Florence Thompson of the TXWSC fethomps@usgs.gov
# Tool developed by David Andresen of the TXWSC dandresen@usgs.gov

import arcpy
from arcpy import env


# Set the current workspace
env.workspace = r"Z:\FAS\FAS_HUC\TEST.gdb"
env.overwriteOutput = True
CountyLayer = "CountyOriginal"
HucLayer = "HUC2000"

# Project the fc's into NAD 1983 Texas Centric Mapping System Albers
# County
inCounty = CountyLayer
CountyPrjtd = "CountyPrjtd"
outProjection = 3083
arcpy.Project_management(inCounty,CountyPrjtd,outProjection)
# HUC
inHuc = HucLayer
HucPrjtd = "HucPrjtd"
arcpy.Project_management(inHuc,HucPrjtd,outProjection)
print ("Projection Complete")

# create a FIPS field in County and make it equal to the last three characters of the GEOID field
arcpy.AddField_management(CountyPrjtd,"FIPS","TEXT")
arcpy.CalculateField_management(CountyPrjtd,"FIPS",
                                '!GEOID! [2:5]' , "PYTHON")
print ("FIPS field created in County")

# Select the HUCs that intersect the state and make a new FC
# Make a layer from the County feature class
HucLyr = "huc_lyr"
arcpy.MakeFeatureLayer_management(HucPrjtd, HucLyr)
# Select all HUCs that intersect Texas Counties
CountyHuc = "CnountyHuc"
arcpy.SelectLayerByLocation_management(HucLyr, "intersect", CountyPrjtd)
arcpy.CopyFeatures_management(HucLyr, CountyHuc)
# Clip the counties to the HUC coastline
CountyClip = "CountyClip"
arcpy.Clip_analysis(CountyPrjtd, CountyHuc, CountyClip)
print ("County clipped to HUC for accuracy")

# add field (double) called MilesSQ to county
arcpy.AddField_management(CountyClip,"MilesSQ","DOUBLE")
# Calculate geometry tool to populate the field with area in sq mi
arcpy.CalculateField_management(CountyClip,"MilesSQ",
                                '!shape.area@SQUAREMILES!', "PYTHON")
print ("County square miles complete")

# Intersect on CountyClip and hucs name it HUC_County_Intersect_20xx
CountyHucIntersect = "HucCountyIntersect" # rename file here- do a find and replace
arcpy.Intersect_analysis([CountyClip,HucPrjtd],CountyHucIntersect,"ALL")
print ("Intersect of County and HUC complete")

# Add field (double) called MilesSQ_2 to HUC_County_Intersect_20xx
arcpy.AddField_management(CountyHucIntersect,"MilesSQ_2","DOUBLE")
# 	Calculate geometry tool to populate the field with area in sq mi
arcpy.CalculateField_management(CountyHucIntersect,"MilesSQ_2",
                                '!shape.area@SQUAREMILES!', "PYTHON")
print ("Second square mile field added")

# Add field (double) called Ratio to HUC_County_Intersect_20xx
arcpy.AddField_management(CountyHucIntersect,"Ratio","DOUBLE")
# Calculate ratio MilesSQ_2/MilesSQ
arcpy.CalculateField_management(CountyHucIntersect,"Ratio",
                                '(!MilesSQ_2!/!MilesSQ!)', "PYTHON")
print ("Calcuated Ratio")

# save the HUC_County_Intersect to a txt file
outPath = r"Z:\FAS\FAS_HUC\MainTxtFiles"
outName = "main1985TEST.txt"
arcpy.TableToTable_conversion(CountyHucIntersect,outPath,outName)
print("Table exported and saved")

print("Complete!")
