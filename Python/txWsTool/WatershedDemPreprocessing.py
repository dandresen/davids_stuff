'''
The following script was developed for DEM pre-processing used for USGS watershed delineations.
It uses watershed processing methods developed by the TXWSC. The script creates four raster
files: Projected-DEM, Fill-DEM, Flow Direction, and Flow Accumulation.

Created by: David Andresen
'''

import arcpy
from arcpy import sa
from arcpy import env

# check out Spatial Analyst
arcpy.CheckOutExtension("Spatial")
# overwrite output
arcpy.overwriteOutput = True
# set workspace
arcpy.env.workspace = arcpy.GetParameterAsText(0)
# name watershed
name = arcpy.GetParameterAsText(1)
# point for snap pour ponit
#point = arcpy.GetParameterAsText(2)
# set mask
arcpy.env.mask = arcpy.GetParameterAsText(2)
# use single DEM
singleDEM = arcpy.GetParameterAsText(3)
# use multiple DEMs
multipleDEMs = arcpy.GetParameterAsText(4)

arcpy.AddMessage("\nThe following script was developed for DEM pre-processing used for USGS watershed delineations. It uses watershed processing methods developed by the TXWSC.\n")

if singleDEM == "":
    arcpy.AddMessage("The script creates five raster files: Mosaic-DEM, Projected-DEM, Fill-DEM, Flow Direction, and Flow Accumulation.\n")
    arcpy.AddMessage("Running the Mosaic to New Raster tool\n")
    mosaicOut = ("mosaic" + name)
    mosaicDEMout = arcpy.MosaicToNewRaster_management(multipleDEMs,env.workspace,mosaicOut,"","32_BIT_FLOAT","","1","LAST","FIRST")
    # project raster to NAD 1983 Albers
    arcpy.AddMessage("Projecting DEM to NAD 1983 Albers")
    projOut = ("prj" + name)
    nad = r"NAD_1983_Albers.prj"
    projectedMosaicDEM = arcpy.ProjectRaster_management(mosaicOut, projOut, nad, "BILINEAR")
    arcpy.AddMessage("prjd" + name + " DEM" + " created\n")
    # fill tool
    arcpy.AddMessage("Running Fill tool")
    outFillMosaic = sa.Fill(projOut)
    outFillMosaic.save("fill" + name)
    arcpy.AddMessage("fill" + name + " DEM" + " created\n")
    # flow direction tool
    arcpy.AddMessage("Running Flow Direction tool")
    outFldMosaic = sa.FlowDirection(outFillMosaic)
    outFldMosaic.save("fld" + name)
    arcpy.AddMessage("fld" + name + " created\n")
    # flow accumulation tool
    arcpy.AddMessage("Running Flow Accumulation tool")
    outFlacMosaic = sa.FlowAccumulation(outFldMosaic)
    outFlacMosaic.save("flac" + name)
    arcpy.AddMessage("flac" + name + " created\n")
    # pour point tool inputs
    # arcpy.AddMessage("Running Snap Pour Point tool\n")
    # outSpp = sa.SnapPourPoint(point, outFlacMosaic, "50")
    # # outSppMosaic.save("spp" + name)
    # # watershed tool
    # arcpy.AddMessage("Running Watershed tool\n")
    # outWs = sa.Watershed(outFldMosaic, outSpp)
    # # wsMosaic.save("wsRas" + name)
    # # raster to polygon tool
    # arcpy.AddMessage("Running Raster to Polygon tool")
    # polyOutName = ("da_" + name)
    # outWsPoly = arcpy.RasterToPolygon_conversion(outWs, polyOutName, "NO_SIMPLIFY", "VALUE")
    arcpy.AddMessage("da_" + name + " created")

else:
    arcpy.AddMessage("The script creates four raster files: Projected-DEM, Fill-DEM, Flow Direction, and Flow Accumulation.\n")
    # project dem to NAD 1983 Albers
    arcpy.AddMessage("Projecting DEM to NAD 1983 Albers")
    projOut = ("prj" + name)
    nad = r"NAD_1983_Albers.prj"
    projectedDEM = arcpy.ProjectRaster_management(singleDEM, projOut, nad, "BILINEAR")
    arcpy.AddMessage("prjd" + name + " DEM" + " created\n")
    # fill tool
    arcpy.AddMessage("Running Fill tool")
    outFill = sa.Fill(projOut)
    outFill.save("fill" + name)
    arcpy.AddMessage("fill" + name + " DEM" + " created\n")
    # flow direction tool
    arcpy.AddMessage("Running Flow Direction tool")
    outFld = sa.FlowDirection(outFill)
    outFld.save("fld" + name)
    arcpy.AddMessage("fld" + name + " created\n")
    # flow accumulation tool
    arcpy.AddMessage("Running Flow Accumulation tool")
    outFlac = sa.FlowAccumulation(outFld)
    outFlac.save("flac" + name)
    arcpy.AddMessage("flac" + name + " created\n")
    # # pour point tool inputs
    # arcpy.AddMessage("Running Snap Pour Point tool\n")
    # outSpp = sa.SnapPourPoint(point, outFlac, "50")
    # # outSppMosaic.save("spp" + name)
    # # watershed tool
    # arcpy.AddMessage("Running Watershed tool\n")
    # outWs = sa.Watershed(outFld, outSpp)
    # # wsMosaic.save("wsRas" + name)
    # # raster to polygon tool
    # arcpy.AddMessage("Running Raster to Polygon tool")
    # polyOutName = ("da_" + name)
    # outWsPoly = arcpy.RasterToPolygon_conversion(outWs, polyOutName, "NO_SIMPLIFY", "VALUE")
    # arcpy.AddMessage("da_" + name + " created")


