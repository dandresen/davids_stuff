'''
The following script can be used to create a watershed from a point vector layer. The result of
the script is a polygon feature class

Created by: David Andresen
'''

import arcpy
from arcpy import sa
from arcpy import env



arcpy.env.workspace =  arcpy.GetParameterAsText(0)
name =  arcpy.GetParameterAsText(1)
point = arcpy.GetParameterAsText(2)
flac =  arcpy.GetParameterAsText(3)
fld =  arcpy.GetParameterAsText(4)

arcpy.AddMessage("\nThe following script was developed to create a watershed from a point vector layer. "
                 "It uses watershed processing methods developed by the TXWSC. The result of the script is a "
                 "polygon feature class.\n")


# pour point tool inputs
arcpy.AddMessage("Running Snap Pour Point tool\n")
outSpp = sa.SnapPourPoint(point,flac,"200")
#outSppMosaic.save("spp" + name)

# watershed tool
arcpy.AddMessage("Running Watershed tool\n")
outWs = sa.Watershed(fld,outSpp)
#wsMosaic.save("wsRas" + name)

# raster to polygon tool
arcpy.AddMessage("Running Raster to Polygon tool")
polyOutName = ("da_" + name)
outWsPoly = arcpy.RasterToPolygon_conversion(outWs,polyOutName,"NO_SIMPLIFY","VALUE")
arcpy.AddMessage("da_" + name + " created")
