import arcpy
from arcpy import sa
from arcpy import env

# check out Spatial Analyst
arcpy.CheckOutExtension("Spatial")

# overwrite output
arcpy.overwriteOutput = True

# set cellsize
arcpy.env.cellSize = "MINOF"

# set workspace
arcpy.env.workspace = arcpy.GetParameterAsText(0)

# name of ws
name = arcpy.GetParameterAsText(1)

# set mask
arcpy.env.mask = arcpy.GetParameterAsText(2)

# add message about tool
arcpy.AddMessage("\nThe results are projected in NAD 1983 Texas Centric Mapping System Albers\n")

# use single in DEM
singleDEM = arcpy.GetParameterAsText(3)

# use multiple DEMs
multipleDEMs = arcpy.GetParameterAsText(4)
if singleDEM == "":
    arcpy.AddMessage("Running the Mosaic to New Raster tool\n")
    mosaicOut = ("mosaic" + name)
    mosaicDEMout = arcpy.MosaicToNewRaster_management(multipleDEMs,env.workspace,mosaicOut,"","32_BIT_FLOAT","","1","LAST","FIRST")

# project raster (or mosaic) to NAD 1983 Albers
arcpy.AddMessage("Projecting DEM to NAD 1983\n")
projOut = ("prj" + name)
nad = 102601
if singleDEM == "":
    projectedMosaicDEM = arcpy.ProjectRaster_management(mosaicOut,projOut,nad,"BILINEAR")
else:
    projectedDEM = arcpy.ProjectRaster_management(singleDEM, projOut, nad, "BILINEAR")

# fill tool
arcpy.AddMessage("Running Fill tool\n")
if singleDEM == "":
    outFillMosaic = sa.Fill(projOut)
    outFillMosaic.save("fill" + name)
else:
    outFill = sa.Fill(projOut)
    outFill.save("fill" + name)

# flow direction tool
arcpy.AddMessage("Running Flow Direction tool\n")
if singleDEM == "":
    outFldMosaic = sa.FlowDirection(outFillMosaic)
    outFldMosaic.save("fld" + name)
else:
    outFld = sa.FlowDirection(outFill)
    outFld.save("fld" + name)

# flow accumulation tool
arcpy.AddMessage("Running Flow Accumulation tool\n")
if singleDEM == "":
    outFlacMosaic = sa.FlowAccumulation(outFldMosaic)
    outFlacMosaic.save("flac" + name)
else:
    outFlac = sa.FlowAccumulation(outFld)
    outFlac.save("flac" + name)
