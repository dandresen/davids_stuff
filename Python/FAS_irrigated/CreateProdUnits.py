# ***********************************START****************************************************************
# The following scripts were used to create production units (grids), mosaic NAIP images, and clip the NAIP images for
# the FAS URGB irrigated acres workflow.
# The grids were separated from one feature class to multiple feature classes. A mosaicked NAIP raster was then clipped
# by the grids.
# The purpose of this data preparation was to prepare NAIP images for a tool that auto generates polygons using object
# oriented delineations based on multispectral imagery
# from the spectral signature of the NAIP images.

# dandresen@usgs.gov

# ************************************END*****************************************************************

import arcpy
from arcpy import env
from arcpy.sa import *
from datetime import datetime

# ***********************************START****************************************************************
# use the fishnet tool to create the grids

# # set the current workspace
# env.workspace = r"D:\fas_scratch\Produnits.gdb"
#
# # name feature class that the fishnet will fit into
# fc = r"SAMPLE2"
# desc = arcpy.Describe(fc)
#
# # run the fishnet tool
# Name = "produnitfoo"
# CellWidth = "18000"
# CellHeight = "14000"
# NumberRows = "70"
# NumberColumns = "25"
# grid = arcpy.CreateFishnet_management(Name, str(desc.extent.lowerLeft), str(desc.extent.XMin)
#                                       + " " + str(desc.extent.YMax ),CellWidth,CellHeight,NumberRows,
#                                       NumberColumns, str(desc.extent.upperRight), "NO_LABELS", "#", "POLYGON")

# ************************************END*****************************************************************


# ***********************************START****************************************************************
# split the grids (fishnet) into single feature classes by unique numeric identifier
# these single grids will be used to clip the mosaic NAIP raster

# StartTime = datetime.now()
# print ("Start Time %s") % StartTime
# arcpy.env.overwriteOutput = True
#
# # name file paths
# inFile = r"D:\fas_scratch\prodNorthFC.gdb\NeededFC"
# outDir = r"D:\fas_scratch\prodNorthFC.gdb\Needed\north"
#
# # reads the fc for different values in the attribute table
# rows = arcpy.SearchCursor(inFile)
# row = rows.next()
# attribute_types = set([])
#
# # name attribute here, ProdNumber is the name of the column with the unique numeric identifier
# while row:
#     attribute_types.add(row.ProdNumber)
#     row = rows.next()
#
# # output a fc for each unique numeric identifier
# for eachAttribute in attribute_types:
#     outFC = outDir + eachAttribute
#     print (outFC)
#     arcpy.Select_analysis(inFile, outFC, "\"ProdNumber\" = '" + eachAttribute + "'") # name attribute here
#
# EndTime = (datetime.now() - StartTime)
# print ("Elapsed Time %s") % EndTime

# ************************************END*****************************************************************


# ***********************************START****************************************************************
# mosaic the NAIP rasters
# StartTime = datetime.now()
# print ("Start Time %s") % StartTime
#
# # name the file where the rasters are located
# arcpy.env.workspace = r"D:\fas_scratch\unzip"
# rasters = arcpy.ListRasters("*", "TIF")
#
# # name the output location
# output = r'D:\fas_scratch\FASmosaic.gdb'
#
# # name the coordinate system
# coordinate_system = arcpy.SpatialReference(26913)
#
# # print the number of rasters to be mosaic
# print (len(rasters))
# arcpy.MosaicToNewRaster_management(rasters, output, "fullMosaic", coordinate_system, "8_BIT_UNSIGNED", "1", "4")
#
# print ("Mosaic Complete")
#
# EndTime = (datetime.now() - StartTime)
# print ("Elapsed Time %s") % EndTime

# ************************************END*****************************************************************

# ***********************************START****************************************************************
# clip the mosaic NAIP raster by each production unit
# each new raster will share the unique numeric identifier with the grid

# StartTime = datetime.now()
# print ("Start Time %s") % StartTime
#
# arcpy.env.workspace = r"D:\fas_scratch\prodNorthFC.gdb\Needed"
# fcs = arcpy.ListFeatureClasses()
#
# print ("Clipping to FC")
#
# for fc in fcs:
#     fullMosaic = r'D:\fas_scratch\FASmosaic.gdb\fullMosaic'
#     output = r"D:\fas_scratch\prodNorthRaster.gdb\Needed\north"
#     rows = arcpy.SearchCursor(fc)
#     row = rows.next()
#     attribute_types = set([])
#     while row:
#             attribute_types.add(row.ProdNumber)  # name attribute here
#             row = rows.next()
#     for eachAttribute in attribute_types:
#             outlocation = output + eachAttribute
#     print (outlocation)
#     arcpy.Clip_management(fullMosaic, '#', outlocation, fc, "#", "ClippingGeometry", "NO_MAINTAIN_EXTENT")
#
# EndTime = (datetime.now() - StartTime)
# print ("Elapsed Time %s") % EndTime

# ************************************END*****************************************************************
