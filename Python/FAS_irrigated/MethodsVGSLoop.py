# ---------------------------------------------------------------------------
# MethodsVGSLoop.py
# Rio Grande Irrigated Acres
# Auto generate polygons using object oriented delineations based on multispectral imagery
# Loops through production units geodatabase and runs the tool keeping a consistent
# unique identifier naming convention
# vstengel@usgs.gov --tool developer
# dandresen@usgs.gov
# ---------------------------------------------------------------------------


# Import arcpy module
import arcpy
from arcpy.sa import *
from datetime import datetime

# Needed File Paths
WorkspaceFolderWithImages = r"D:\FAS_IrrAcr_ProdUnit\ProdSouthRaster.gdb"
OutputFolderNonNeededRasters = r"D:\FAS_IrrAcr_ProdUnit\DontNeedRas.gdb"
OutputFolderNonNeededVectors = r"D:\FAS_IrrAcr_ProdUnit\DontNeedVec.gdb"
OutputFolderNeededVectors = r"D:\FAS_IrrAcr_ProdUnit\South.gdb"

# No need to modify below this line

# Overwrite pre-existing files
arcpy.env.overwriteOutput = True

# Local variables:
arcpy.env.workspace = WorkspaceFolderWithImages
ProdRast = arcpy.ListRasters("*")

# SimpPoly_Pnt = "D:\\RioGrande\\Test\\TestModel\\SimpPolyPnt11.shp"
# The above output is not in this code.

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Run the tool in a loop
for raster in ProdRast:
    arcpy.env.overwriteOutput = True
    StartTime = datetime.now()
    print ("Start Time %s") % StartTime

    # name of the production unit
    print (raster)

    Seg = OutputFolderNonNeededRasters + "\\" + "Seg" + "".join([str(i) for i in raster])
    Poly = OutputFolderNonNeededVectors + "\\" + "Poly" + "".join([str(i) for i in raster])
    Elim = OutputFolderNonNeededVectors + "\\" + "Elim" + "".join([str(i) for i in raster])

    # Process: Segment Mean Shift
    arcpy.gp.SegmentMeanShift_sa(raster, Seg, "5", "15", "20", "4 3 2")
    print ("Segment Mean Shift")

    # Process: Raster to Polygon
    arcpy.RasterToPolygon_conversion(Seg, Poly, "SIMPLIFY", "COUNT")
    print ("Raster to Polygon")

    # Process: Eliminate Polygon Part
    arcpy.EliminatePolygonPart_management(Poly, Elim, "AREA_AND_PERCENT", "40000 SquareMeters", "75", "ANY")
    print ("Eliminate Polygon Part")

    Output = OutputFolderNeededVectors + "\\" + raster

    # Process: Simplify Polygon
    arcpy.SimplifyPolygon_cartography(Elim, Output, "POINT_REMOVE", "10 Meters", "50 SquareMeters",
                                      "NO_CHECK", "KEEP_COLLAPSED_POINTS")
    print ("Simplify Polygon")

    # output file folder
    Output = OutputFolderNeededVectors + "\\" + raster
    print (Output)

    # timer
    EndTime = (datetime.now() - StartTime)
    print ("Elapsed Time %s") % EndTime


