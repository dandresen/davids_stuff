'''
The script below can be used to clean twitter analytics data. The output of the script is an excel file
and CSV's with lat long.

Created by: David Andresen
'''

# import needed libraries
import pandas as pd
import functools as ft
import os

####### CHANGE PATH AND FILE NAMES BELOW!!!! ########
# name the file path
path  = r"\\Igskiacwgsnas\TX Pickup\dandresen\AnalyticsMaps\python"
# name the output excel file
excelFile = pd.ExcelWriter(r"twitter_out.xlsx")
# name the Lat Long file
latlongFile = r"LatLongMaster.csv"
####### CHANGE PATH AND FILE NAMES ABOVE!!!! ########



#############################################################################################
# the line below says: "in the defined path, give me every file that starts with tweet"
files = [filename for filename in os.listdir(path) if filename.startswith("tweet")]
# once the files have been identified (line above) run the clean-up scripts and create the Excel file and lat long CSVs
print("Cleaning up the data...\n")
print("Found these files:\n")
# this part takes every file that starts with "tweet" and massages the data to how we need it
for f in files:
    print (f)
    # below is a way to name the excel workbooks- it takes the 28th to the 38th characters in the original CSV file names (Flood and Rain_)
    sheet = str(f[28:47])
    # read in the needed CSVs
    df = pd.DataFrame(pd.read_csv(str(f), parse_dates=[0], infer_datetime_format=True))
    df = df.rename(columns ={'Tweet text': "StationName"}) # change this column name to make life easier
    # the line below extracts the station number from the column StationName and creates a new column, StationID
    df['StationID'] = df.StationName.str.extract('(\d+)', expand=True).astype(str)
    # split up date and time column into their own columns
    tempDf = pd.DatetimeIndex(df['time'])
    df["Date"]= tempDf.date
    df["Time"]= tempDf.time.astype(str) # make this a string b/c import into excel
    del df['time']
    # write a the comments to their own workbook
    df_Comment = df[~df['StationName'].str.contains("-")] # the '~' indicates the opposite
    df_Comment = pd.DataFrame(df_Comment[['StationID','StationName','Date','Time','impressions', 'engagements','engagement rate','retweets','replies','likes']])
    df_Comment.to_excel(excelFile,sheet_name=sheet + "_Comment",index=False) # export the comment workbooks
    # the line below cleans up the column StationName by extracting the needed information
    df['StationName'] = df.StationName.str.extract('#([^/]+)TX', expand=True).astype(str)
    df['StationName'] = df['StationName'].astype(str) + " TX" # add TX back to the rows
    # below is where you can choose what columns you want to print in the final excel file
    df = pd.DataFrame(df[['StationID','StationName','Date','Time','impressions', 'engagements','engagement rate','retweets','replies','likes']])
    # select a row from StationName column with a (-) & get the site number & station name
    # rows missing a (-) will not be used
    df_noComment = df[df['StationName'].str.contains("-")]
    # export to excel
    df_noComment.to_excel(excelFile,sheet_name=sheet,index=False)

    # create the lat long CSVs
    df_latlong = pd.read_csv(latlongFile,converters={'StationID':str})
    # join the lat long master csv to the rain and flood dataframes
    join_df = df.merge(df_latlong,on="StationID",how="left")
    # count the StationName and name the column tweets
    df_twet = df.groupby(['StationID'])['StationID'].size().astype(int).reset_index(name='Tweets')
    # sum the impressions
    df_imp = df.groupby(['StationID'])['impressions'].sum().astype(int).reset_index(name='Impressions')
    # sum the engagements
    df_eng = df.groupby(['StationID'])['engagements'].sum().astype(int).reset_index(name='Engagements')
    # group all of the data frames that are going to make the csv
    dfs = [df_latlong,df_twet,df_imp,df_eng]
    # combine all of the data frames for the csv
    df_csv_final = ft.reduce(lambda left,right: pd.merge(left,right,on='StationID'),dfs)
    # make sure to turn the output into a data frame
    df_csv_out = pd.DataFrame(df_csv_final)
    name = ("LatLong" + sheet + ".csv") # same naming convention as above, just added "LatLong" to it
    # export lat long to CSVs
    pd.DataFrame.to_csv(df_csv_out,name,index=False)
excelFile.save()
print("\nExcel file and Lat Long CSVs created\n")



##### scratch code, some of this might be useful?? if not you can delete it
# file = (r"D:\python\Twitter\new_twitter_rain.csv")
# df = pd.DataFrame(pd.read_csv(file))
# df = df.rename(columns={'site_no':'site_info'})
# df['site_no'] = df.site_info.str.extract('(\d+)', expand=True).astype(str)
# #join_df = df.merge(pd.read_csv(r"D:\python\Twitter\LatLongMaster_Keep.csv"),on="site_no",how="left")
# pd.DataFrame.to_csv(df,r"D:\python\Twitter\Keep_rain.csv",index=False)
#
# add leading zero to lat long master
# df = pd.read_csv(r"LatLongMaster.csv")
# df['StationID']= df['StationID'].astype(str).str.zfill(8) # only works on site_no with 8 values... did the other 10 sites manually
# df = pd.DataFrame(df.sort_values(by='StationID'))
# pd.DataFrame.to_csv(df,'newlatlong.csv',index=False)
# print(df)
