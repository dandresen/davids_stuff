import pandas as pd

# path to the main text file with the ratios (result of the GIS process)
txtIn = r"Z:\FAS_HUC\MainTxtFiles\main2000.txt"
# path to the County water data Excel file 
countyIn = r"Z:\FAS_HUC\CountyTables\Export_2010, Texas_County.xlsx"

# readin the 'main' file
main = pd.DataFrame(pd.read_csv(txtIn,converters={'FIPS': str,'HUC8': str}))
main['Area'] = main['FIPS'] # change column name 'FIPS' to 'Area'
ratiodf1 = main[['Area', 'Ratio','HUC8']] # do I need to make two here??
huc8 = main[['Area','HUC8']]
# this reads in the County excel file, 'sheetname=None' is the way to read in every workbook
# 
countydf = pd.read_excel(countyIn, sheetname=None, skiprows=3, skip_footer=5, converters={'Area':str}) # don't wrap in pd.DataFrame()!
# since the workbooks are now a dictionary, the below loop can get the data from each workbook
for df, data in countydf.items():
    newdf_1 = data.ix[0:,1:].copy()

# need to get the math part to work before its put in a for loop 
newdf = countydf['PS'].merge(ratiodf1,'inner','Area')
math = newdf.loc[:,1:].copy().multiply(newdf['Ratio'],axis="index") # error with this...
#group = math.groupby(newdf['HUC8']).sum()
print (math)



